---
layout: markdown_page
title: "Learning and Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction
{: #introduction}

Welcome to the Learning & Development (L&D) page! L&D is an essential part of any organization's growth, success, and overall business strategy. We want to support the development of GitLabbers' competencies, skills and knowledge by providing the right tools and opportunities to progress their own personal and professional development.

One way to promote development is to offer courses designed to add or improve knowledge in areas such as business and financial acumen, communication, management, organization, and productivity. We chose to implement a learning management system (LMS), Grovo, to help us manage the design process and delivery of content for these and other topics of interest.

## Grovo
{:.no_toc}

### What is Grovo?

Grovo is a Learning Management System (LMS) designed to help us create and deliver short, engaging learning tracks (courses) focused on a specific learning opportunity. Whether you're on your desktop, laptop, tablet, or phone, you can start and stop a course and pick up where you left off at any time. Grovo's content is delivered using a method called Microlearning.

### What is Microlearning?

Microlearning provides short bursts of information that can be applied right away, helping people build the skills they need to be successful at their jobs. Think of these bite-size pieces of learning like sentences: each conveys just one complete thought, but when you put a few of them together, you build a more complex idea. Each microlearning moment is made of a digestible morsel of information combined with a short practice exercise. It's hyper specific, so you can get the right help, right when you need it: like when composing an email, working with new spreadsheet software, or practicing a big speech. Bit by bit, these microlearning moments add up to better job performance today and continual improvement going forward. Microlearning isn't just breaking up content into smaller pieces. It's a way to make learning digestible and effective, transforming not just what you know, but what you actually do, every day.

### Grovo at GitLab

We're a remote and globally distributed team that requires something more than the traditional learning approaches and courses that currently exist. We want to foster a new way of learning that mirrors GitLab's culture.

Take a coffee break with Grovo and see how much you can learn. It can take just under 60 seconds to sharpen your skills!

### The Quizzes

Quizzes are a tool used to test understanding and transfer of knowledge. After each micro learning lesson in Grovo there are usually two quiz questions at the end which have been designed to engage and reinforce what the learner has just heard.  You can read more about the science behind quizzes in [Creating a Learning Environment Using Quizzes](https://www.td.org/Publications/Blogs/L-and-D-Blog/2013/09/Creating-a-Learning-Environment-Using-Quizzes) by Swati Karve for the [Association for Talent Development](https://www.td.org/)
Following the initial introduction of Grovo we asked for feedback and the majority of that was about the quizzes. We have now taken them out of the individual lessons and put them right at the end of the chapter(s). This should make it a much more smoother experience. Once you click Start Training, you just sit back and learn!


### Frequently Asked Questions (FAQs)

**Q** If I receive a message to move to the next chapter, does that mean I got 100% on the quiz?

**A** No, it means you have completed and finished this chapter

**Q** What happens if I don’t achieve the required percentage to pass?

**A** Don't worry :) You can take the course again by pinging us (the people ops team). We will be able to assign a retake for you so you can have another go

 **Q** How do I retake a quiz?

**A** It is not possible to just retake the quiz by itself. You will need to take the course again which you can do by letting people ops know the course name and we can reassign it to you.

**Q** How do I review the results of the quiz I just took?

**A** If you're taking a course with no chapters, after clicking submit you will be able to immediately review your answers to the quiz you have just taken.
If the course has multiple chapters, after clicking Submit, click on the X at the top right of the message box to review your quiz responses and clarify the correct answers for any questions you missed. When you have finished reviewing the quiz results, click Next to advance to the next chapter.

**Q** Can I see which questions I got wrong or right in detail?

**A** Yes, once you have completed the entire course along with the quiz, a message will appear scroll down and you can see the questions and if you got one wrong you can click on the "details" link on the right hand side of the question to reveal the correct answer.


### Course Listings
{:.no_toc}

The course times below are estimates which also include the time to complete a quiz.


### For Individual Contributors

IC 004 Social Media (28 mins)

IC 120 Building Effective Communication Skills (13 mins)

IC 130 Collaboration & Consensus (8 mins)

IC 140 Productivity Under Pressure (10 mins)

IC 141 Effective Productivity (30 mins)

IC 142 Staying Focused (34 mins)

IC 143 How to Manage Projects (36 mins)

### For Managers

MGR 100 The Role of a Manager (15 mins)

MGR 101 Develop yourself as a Manager (1hr)

MGR 120 Communicating Effectively (18 mins)

MGR 140 Productivity Under Pressure (12 mins)

MGR 160 Managing Performance Issues (12 mins)

MGR 161 Develop Your Team (31 mins)

MGR 162 Motivate & Enable Your Team (43 mins)

MGR 165 Self Improvement & Team Dynamics (31 mins)

MGR 170 Financial Fundamentals (18 mins no quiz)

MGR 200 Strategic Management (35 mins)

MGR 210 Fostering Creativity & Innovation (44 mins)
