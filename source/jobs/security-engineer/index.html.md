---
layout: job_page
title: "Security Engineer"
---

We are looking for a capable security engineer who is able to identify and fix
vulnerabilities within the GitLab software stack and help implement good
security practices within the company.

## Responsibilities

* Find and fix security issues within the GitLab code base
* Define, implement, and monitor security measures to protect GitLab.com and company assets
* Manage a [bug bounty program](https://medium.com/@collingreene/bug-bounty-5-years-in-c95cda604365#.blaaokpi9)
* Perform vulnerability testing, risk analyses, and security assessments
* Investigate intrusion incidents, conduct forensic investigations, and mount incident responses
* Collaborate with colleagues on authentication, authorization and encryption solutions
* Evaluate new technologies and processes that enhance security capabilities
* Analyze and advise on new security technologies and program conformance
* Write documentation around how to maintain a high-level of security.

## Lead

As a lead of the security team you should organize the work to keep the trust of our users:

1. Make a risk assessment (based on ISO 27001 but we don't seek certification)
1. Define priorities, scope, and a time-line
1. Define a hiring plan, budget, and internal cooperation you need.
1. Make sure the plan is always up to date and links to the relevant issues
1. Guard the time-line and escalate when needed

The actions can be on the following fronts:

- Application security engineer that gets others excited about security and runs the bud bounty program (1 person)
- Secure Development Life-cycle (SDL) process guidance
- Style guides and design best practices for engineering
- Courses for engineering (including guest speakers)
- Reduce surface area in application (Git Annex, old API)
- Post-postmortems on found security bugs (helps people think about it, high leverage)
- Document security trade-offs
- Automated testing/linting
- Compliance (HIPAA)
- Offensive (pen testing)
- Detection and response (monitoring, Detection, IDS, OSSEC, updates, response), 1 person that can also be the lead
- Network security (teleport, VPC's, firewalls, access control, also IDS), 1 person in the production team
- Patch management / Vulnerability management and coordination (modeled after relevant ISO standard)
- Defense in depth recommendations
- Penetration testing by externals
- Source code analysis by externals
- Bug bounty program
- Endpoint security (fleetsmith, encryption, phishing reporting, yubikey, reducing access), 1 person in production team
- Runbooks for incidents, recovery plans
- Abuse (spam, bitcoin mining)
- Package infrastructure/update/release process/patches
- Communication (blog post, postmortems, incident response/crisis communication)
- Dependencies and contribution security risks
- Credential management (Vault)

We agree that [Good Security Is Holistic](https://medium.com/@justin.schuh/stop-buying-bad-security-prescriptions-f18e4f61ba9e). We think that simulating a security culture in engineering is one of the most important things. We don't do checklist security, the goal is to keep the trust of our users by being secure, compliance is not a goal in itself. We don't think that third party products are important but they are no silver bullet to make everything secure.

## Requirements for Applicants

* Significant computer security experience in production-level settings
* Passion for open source
* Linux experience (e.g. Ubuntu)
* Programming experience (Ruby and Ruby on Rails preferred; for GitLab debugging)
* Collaborative team spirit with great communication skills
* You share our [values](/handbook/values), and work in accordance with those values.
